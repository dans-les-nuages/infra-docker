#!/bin/bash

usage() {
  echo -e "Need a .env file where you are ($(pwd)) to run the script.\nExiting."
  exit 1
}

[ -f .env ] && source .env || usage

echo "Archive $DOMAIN, please wait ..."
time tar -czf $BKPFILE $PRDDIR
echo "Backup $PRDDIR, please wait ..."
time rclone sync -PL $BKPFILE C:Backups/$PRDDIR
echo "Check $PRDDIR, please wait ..."
time rclone check $BKPFILE C:Backups/$PRDDIR

rm $BKPFILE

echo "Archive $DOMAIN, please wait ..."
time tar -czf $BKPFILE $DOCDIR
echo "Backup $DOCDIR, please wait ..."
time rclone sync -PL $BKPFILE C:Backups/$DOCDIR
echo "Check $DOCDIR, please wait ..."
time rclone check $BKPFILE C:Backups/$DOCDIR

rm $BKPFILE

echo "Done!"
