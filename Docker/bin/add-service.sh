#!/bin/bash

usage() {
  echo -e "usage: $(basename $0) servicename"
  exit 1
}

exitenv() {
  echo -e "Need to be in docker-adminrezo's project with a .env file\nExiting."
  exit 1
}

SVC=$1
[ ! $SVC ] && usage
[ -f .env ] && source .env || exitenv
git remote get-url origin |grep 'infra-as-code' || exitenv

echo "Add dockerfile"
read
cp docker-compose.yml.TEMPLATE docker-compose.${SVC}.yml

echo "Modify dockerfile"
read
vi docker-compose.${SVC}.yml "+/%%%"

echo "Modify shadow's dockerfile"
read
vi docker-compose.yml

echo "Add nginx conf"
read
cp VOLUMES/shadow-conf/conf.d/TEMPLATE.conf.tpl VOLUMES/shadow-conf/conf.d/${SVC}.conf

echo "Modify nginx conf"
read
vi VOLUMES/shadow-conf/conf.d/${SVC}.conf

echo "Modify .env"
read
vi .env

echo "Reloading Docker Compose"
docker compose up -d

sleep 3s
clear
echo "Here you are"
docker compose ps




