#!/bin/bash

usage() {
  echo -e "usage: $(basename $0) /dir/to/docker"
  exit 1
}

exitenv() {
  echo -e "Need a .env file where you are ($(pwd)) to run the script.\nExiting."
  exit 1
}

DKDIR="$1"

[ ! $DKDIR ] && usage
[ -d $DKDIR ] && cd $DKDIR
[ -f .env ] && source .env || exitenv

docker compose down
docker system prune -f
#git pull --recurse-submodules
docker compose pull
docker compose build
docker compose up -d
