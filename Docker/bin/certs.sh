#!/bin/bash

usage() {
  echo -e "usage: $(basename $0) renew|certonly /dir/to/docker"
  exit 1
}

exitenv() {
  echo -e "Need a .env file where you are ($(pwd)) to run the script.\nExiting."
  exit 1
}

exitact() {
  echo "Bad action $ACTION (need renew or certonly)"
  exit 1
}

DKDIR="$2"
[ ! $DKDIR ] && usage
[ -d $DKDIR ] && cd $DKDIR
[ -f .env ] && source .env || exitenv

ACTION=$1
if [ "$ACTION" != "renew" ] && [ "$ACTION" != "certonly" ]
then
  exitact
fi
[ "$ACTION" = "certonly" ] && ACTION="$ACTION -d $DOMAIN -d *.$DOMAIN -d $DOMAIN2 -d *.$DOMAIN2 --expand"

docker run --rm --name certbot \
	-v $DKDIR/TLS/etc:/etc/letsencrypt \
	-v $DKDIR/TLS/var:/var/lib/letsencrypt \
	certbot/dns-ovh:arm32v6-latest \
		$ACTION \
		--dns-ovh --non-interactive --agree-tos \
		--dns-ovh-credentials /etc/letsencrypt/ovh-api \
		--email contact@$DOMAIN
		

