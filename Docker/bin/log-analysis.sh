#!/bin/bash

cd $DKDIR
rm /tmp/log[1-3] 2> /dev/null

# Get logs in log1
docker compose logs shadow > /tmp/log.full

# Keep only IPs in log2
awk -F"\| " '{print $2}' /tmp/log.full \
	|cut -d" " -f1 |sort \
	|grep -v "^2022\|docker-entrypoint\|10-listen-on-ipv6-by-default" > /tmp/log2

# Unique IPs are in log3
sort -u /tmp/log2 > /tmp/log3

# Find number of hits for each IP
for i in $(cat /tmp/log3)
do
	a=$(grep $i /tmp/log2 |wc -l)
	echo "$a:$i" >> /tmp/log1
done

# Show this inverse sorted
sort -rn /tmp/log1 > /tmp/log.sorted
less /tmp/log.sorted

rm /tmp/log[1-3]
echo "Analysis is in /tmp/log.sorted"
echo "Full log is in /tmp/log.full"
