# My Docker Architecture

## Build

- Dependencies :
  - A very lite computer under Linux (tested on Debian and Raspbian)
  - docker
  - docker-compose
  - git
  - rclone
  - ufw or some other firewall (please, please, let only 80, 443 and the strictly necessary ports opened)

- `git clone THISREPO`
- Copy .env.CHANGEME to .env and set your working vars.
- Go build the stuff :
```
git submodule update --remote --merge
docker-compose pull
docker-compose build
bin/pull-all.sh # Read and change it before
```

## Run

```
docker-compose up -d
```

- Create a crontab like this (use `crontab -e`) :

```
PRDDIR="/home/me/myproddir"
LOGDIR="$PRDDIR/LOGS"
# m h  dom mon dow   command
*/15 * * * * cd $PRDDIR && ./bin/pull-all.sh > $LOGDIR/pull-all.log 2&>1
0 7 * * * $PRDDIR/bin/backup-me.sh > $LOGDIR/backup-me.sh 2&>1
0 7 1 * * $PRDDIR/bin/certs.sh renew > $LOGDIR/certs.log 2&>1
30 23 * * * $PRDDIR/bin/rebuild-all.sh > $LOGDIR/rebuild-all.log 2&>1
```

- Backups (`bin/backup-me.sh`)
  - Create a (ciphered) backup destination with [https://rclone.org/](Rclone) named "C"
- Certs (`bin/certs.sh`)
  - Uses Certbot as a Docker container with OVH DNS plugin. Please change for your own.
