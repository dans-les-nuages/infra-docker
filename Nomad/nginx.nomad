job "nginx" {
  datacenters = ["dc1"]
  type = "service"
  update {
    stagger = "5s"
    max_parallel = 1
  }
  group "nginx" {
    count = 1
    network {
      port "http" { static = 80 }
      port "https" { static = 443 }
    }
    volume "nginx-conf" {
      type      = "host"
      source    = "/home/nico/Devs/dans-les-nuages.fr/VOLUMES/nginx-conf/"
      read_only = true
    }

    task "nginx-docker" {
      driver = "docker"
      config {
        image = "nginx:alpine"
        ports = ["http","https"]
      }
      resources {
        cpu = 200
        memory = 32
      }
    }
  }
}
