# frozen_string_literal: true

# -*- mode: ruby -*-
# vi: set ft=ruby :

$prov_k8s = <<~SCRIPT
  apt update
  apt -y install curl git zsh snapd
  snap install core
  snap install microk8s --classic
  usermod -a -G microk8s vagrant
  newgrp microk8s
  su -c "mkdir /home/vagrant/.kube" vagrant
  su -c "/snap/bin/microk8s kubectl config view > /home/vagrant/.kube/config" vagrant
SCRIPT

$prov_master = <<~SCRIPT
  /snap/bin/microk8s enable dns
  /snap/bin/microk8s enable helm
  /snap/bin/microk8s enable ingress
  /snap/bin/microk8s enable metallb:192.168.56.200-192.168.56.210
  /snap/bin/microk8s enable registry
  /snap/bin/microk8s enable storage
  sed -i "$ a alias m='microk8s'" /home/vagrant/.bashrc
  sed -i "$ a alias kubectl='microk8s.kubectl'" /home/vagrant/.bashrc
  sed -i "$ a alias k='microk8s.kubectl'" /home/vagrant/.bashrc
  sed -i "$ a alias helm='microk8s.helm'" /home/vagrant/.bashrc
  /snap/bin/microk8s kubectl completion bash >/etc/bash_completion.d/kubectl
  sed -i "$ a cd /vagrant" /home/vagrant/.bashrc
SCRIPT

Vagrant.configure('2') do |config|
  config.vm.box = 'debian/bullseye64'
  config.vm.provision 'shell', inline: $prov_k8s
  config.vm.provider 'libvirt' do |v|
    v.memory = 2048
    v.cpus = 2
  end

  config.vm.define :master do |master|
    master.vm.network :private_network, ip: '192.168.56.10'
    master.vm.hostname = 'master1'
    master.vm.provision 'shell', inline: $prov_master
  end

  config.vm.define :node do |node|
    node.vm.network :private_network, ip: '192.168.56.11'
    node.vm.hostname = 'node1'
  end
end
