provider "google" {
  project     = var.project_name
  region      = var.region
  zone        = var.zone
}

resource "google_project_service" "cloudrunapi" {
  service = "run.googleapis.com"
  disable_on_destroy = true
}
