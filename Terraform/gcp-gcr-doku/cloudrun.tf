# Enable CR API

resource "google_project_service" "gcrapi" {
  service                 = "run.googleapis.com"
  disable_on_destroy      = true
}

# Create a Wordpress instance
resource "google_cloud_run_service" "nxcl" {
  depends_on              = [google_project_service.gcrapi]
  name                    = "nxcl"
  location                = var.location

  template {
    spec {
      containers {
        image             = "${var.region}-docker.pkg.dev/${var.project_name}/${var.registry}/${var.image}:${var.img_version}"
        ports {
          container_port = 80
        }
        resources {
          limits          = { "memory" = "1024Mi" }
        }
      }
    }
  }

  traffic {
    percent               = 100
    latest_revision       = true
  }

}
