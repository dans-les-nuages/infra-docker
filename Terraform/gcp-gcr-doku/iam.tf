# Publish your Wordpress publicly

resource "google_cloud_run_service_iam_member" "nxcl_allusers" {
  service  = google_cloud_run_service.nxcl.name
  location = var.region
  role     = "roles/run.invoker"
  member   = "allUsers"
}
