resource "google_compute_global_address" "ip_address" {
  name = "arz-public-ip"
}

resource "google_compute_backend_bucket" "static_sites" {
  name        = "sws-be-bucket"
  description = "Static Web sites bucket backend"
  bucket_name = google_storage_bucket.static_sites.name
}

# Create url map
resource "google_compute_url_map" "default" {
  name = "sws-urlmap"
  default_service = google_compute_backend_bucket.static_sites.id

  host_rule {
    hosts        = ["adminrezo.fr"]
    path_matcher = "sws-path-matcher"
  }
  path_matcher {
    name            = "sws-path-matcher"
    default_service = google_compute_backend_bucket.static_sites.id

    path_rule {
      paths   = ["/*"]
      service = google_compute_backend_bucket.static_sites.id
    }
  }
}

# Create HTTP target proxy
resource "google_compute_target_http_proxy" "default" {
  name    = "http-lb-proxy"
  url_map = google_compute_url_map.default.id
}

# Create forwarding rule
resource "google_compute_global_forwarding_rule" "default" {
  name                  = "http-lb-forwarding-rule"
  ip_protocol           = "TCP"
  load_balancing_scheme = "EXTERNAL_MANAGED"
  port_range            = "80"
  target                = google_compute_target_http_proxy.default.id
  ip_address            = google_compute_global_address.ip_address.id
}

#module "gce-lb-https" {
#  source              = "GoogleCloudPlatform/lb-http/google"
#  version             = "~> 9.0"
#
#  project             = var.project_name
#  name                = var.lb_name
#  enable_ipv6         = true
#  create_ipv6_address = true
#  ssl_certificates    = [var.cert_name]
#
#
#  target_tags       = [module.mig1.target_tags, module.mig2.target_tags]
#  backends = {
#    default = {
#      description                     = null
#      port                            = var.service_port
#      protocol                        = "HTTP"
#      port_name                       = var.service_port_name
#      timeout_sec                     = 10
#      enable_cdn                      = false
#      custom_request_headers          = null
#      custom_response_headers         = null
#      security_policy                 = null
#      compression_mode                = null
#
#      connection_draining_timeout_sec = null
#      session_affinity                = null
#      affinity_cookie_ttl_sec         = null
#
#      health_check = {
#        check_interval_sec  = null
#        timeout_sec         = null
#        healthy_threshold   = null
#        unhealthy_threshold = null
#        request_path        = "/"
#        port                = var.service_port
#        host                = null
#        logging             = null
#      }
#
#      log_config = {
#        enable = true
#        sample_rate = 1.0
#      }
#
#      groups = [
#        {
#          # Each node pool instance group should be added to the backend.
#          group                        = var.backend
#          balancing_mode               = null
#          capacity_scaler              = null
#          description                  = null
#          max_connections              = null
#          max_connections_per_instance = null
#          max_connections_per_endpoint = null
#          max_rate                     = null
#          max_rate_per_instance        = null
#          max_rate_per_endpoint        = null
#          max_utilization              = null
#        },
#      ]
#
#      iap_config = {
#        enable               = false
#        oauth2_client_id     = null
#        oauth2_client_secret = null
#      }
#    }
#  }
#}
