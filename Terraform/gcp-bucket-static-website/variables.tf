variable "gcp_creds" {
  default = ".creds"
  type    = string
}

variable "project_name" {
  default = "manifest-pulsar-415321"
  type    = string
}

variable "region" {
  default = "europe-west1"
  type    = string
}

variable "zone" {
  default = "europe-west1-a"
  type    = string
}

variable "location" {
  default = "EUROPE-WEST1"
  type    = string
}

variable "network_name" {
  default = "sws-vpc"
  type    = string
}

variable "subnet_name" {
  default = "sws-sub"
  type    = string
}

variable "subnet_range" {
  default = "10.0.1.0/24"
  type    = string
}

variable "bucket_name" {
  default = "sws-bucket"
  type    = string
}

variable "lb_name" {
  default = "sws-lb"
  type    = string
}

variable "lb_port" {
  default = 443
  type    = number
}

variable "lb_port_name" {
  default = "sws-lb-https"
  type    = string
}

