resource "google_storage_bucket" "static_sites" {
  name          = var.bucket_name
  location      = var.location
  force_destroy = true

  uniform_bucket_level_access = true

  website {
    main_page_suffix = "index.html"
    not_found_page   = "404.html"
  }
  cors {
    method          = ["GET", "HEAD", "PUT", "POST", "DELETE"]
    response_header = ["*"]
    max_age_seconds = 86400
  }
}

resource "google_storage_bucket_iam_binding" "everyoneview" {
  bucket  = var.bucket_name
  role    = "roles/storage.objectViewer"
  members = [
    "allUsers",
  ]
}

