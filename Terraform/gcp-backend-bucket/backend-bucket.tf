#resource "google_kms_key_ring" "mykeyring" {
#  name     = "bucket-keyring"
#  location = "global"
#}
#
#resource "google_kms_crypto_key" "backend-bucket-key" {
#  name            = "backend-bucket-key"
#  key_ring        = google_kms_key_ring.mykeyring.id
#  rotation_period = "7776000s"
#
#  lifecycle {
#    prevent_destroy = true
#  }
#}

resource "random_id" "bucket_prefix" {
  byte_length = 8
}

resource "google_storage_bucket" "default" {
  name          = "${random_id.bucket_prefix.hex}-bucket-tfstate"
  force_destroy = false
  location      = "EUROPE-WEST1"
  storage_class = "STANDARD"
  versioning {
    enabled = true
  }
#  encryption {
#    default_kms_key_name = google_kms_crypto_key.backend-bucket-key.id
#  }
  depends_on = [
    google_project_iam_member.project
  ]
}

resource "google_project_iam_member" "project" {
  project = var.project_name
  role    = "roles/editor"
  member  = "user:nicolas.dewaele.perso@gmail.com"
}

