resource "docker_container" "shadow" {
  image             = docker_image.proxy.image_id
  name              = "shadow"
  hostname          = "shadow"
  restart           = "on-failure"
  ports {
    internal        = 80
    external        = 80
  }
  ports {
    internal        = 443
    external        = 443
  }
  env               = [
                        "ENABLE_IPV6=true",
                        "SSL_POLICY=Mozilla-Modern"
                      ]
  volumes {
    container_path  = "/tmp/docker.sock"
    volume_name     = "/var/run/docker.sock"
    read_only       = true
  }
  volumes {
    container_path  = "/etc/nginx/certs"
    volume_name     = "${var.volume_path}certs-data"
    read_only       = true
  }
}
