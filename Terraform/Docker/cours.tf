resource "docker_container" "cours" {
  image               = docker_image.nginx.image_id
  name                = "cours"
  hostname            = "cours"
  restart             = "on-failure"
  env                 = ["VIRTUAL_HOST=cours.adminrezo.fr"]
  volumes {
    container_path    = "/var/www/cours"
    volume_name       = "${var.volume_path}cours-data"
    read_only         = true
  }
  volumes {
    container_path    = "/etc/nginx/conf.d"
    volume_name       = "${var.config_path}cours"
    read_only         = true
  }
}
