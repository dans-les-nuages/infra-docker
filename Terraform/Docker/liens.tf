resource "docker_container" "liens" {
  image               = docker_image.shaarli.image_id
  name                = "liens"
  hostname            = "liens"
  restart             = "on-failure"
  env                 = [
                          "VIRTUAL_HOST=liens.adminrezo.fr",
                          "TZ=Europe/Paris"
                        ]
  volumes {
    container_path    = "/var/www/shaarli/cache"
    volume_name       = "${var.volume_path}liens-cache"
    read_only         = false
  }
  volumes {
    container_path    = "/var/www/shaarli/data"
    volume_name       = "${var.volume_path}liens-data"
    read_only         = false
  }
}
