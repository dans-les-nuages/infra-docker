resource "docker_container" "livres" {
  image               = docker_image.php.image_id
  name                = "livres"
  hostname            = "livres"
  restart             = "on-failure"
  env                 = ["VIRTUAL_HOST=livres.adminrezo.fr"]
  volumes {
    container_path    = "/var/www/html"
    volume_name       = "${var.volume_path}livres-data"
    read_only         = false
  }
}
