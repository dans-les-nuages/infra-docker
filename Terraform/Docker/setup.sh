#!/bin/bash

RED='\033[0;33m'
GREEN='\033[0;32m'
NC='\033[0m'
function exitme() { echo -e "${RED}$*${NC}"; exit 255; }
[ -f "variables.tf.sample" ] || exitme "This script needs to be run in the 'terraform' directory of the project"

# Edit variables
echo -e "${GREEN}Edit project variables [Enter to continue]${NC}"
read -r 
[ -e "$EDITOR" ] || EDITOR="vi"
[ -f "variables.tf" ] || cp variables.tf.sample variables.tf
$EDITOR "variables.tf"

# Create volumes
VOLUMES="$(grep 'var.volume_path' -- *.tf | awk -F'"' '{print $2}'  |cut -d'}' -f2)"
echo -e "${GREEN}Create those directories if needed [Enter to continue]${NC}"
echo "  - VOLUMES"
for i in $VOLUMES;do echo "  - VOLUMES/$i"; done 
read -r 
[ -d VOLUMES ] || mkdir VOLUMES
for i in $VOLUMES
do
  [ -d "VOLUMES/$i" ] || mkdir "VOLUMES/$i"
done

# Terraform
echo -e "${GREEN}Terraform Plan [Enter to continue]${NC}"
read -r 
terraform plan
