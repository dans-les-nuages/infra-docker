resource "docker_container" "wiki" {
  image               = docker_image.nginx.image_id
  name                = "wiki"
  hostname            = "wiki"
  restart             = "on-failure"
  env                 = ["VIRTUAL_HOST=wiki.adminrezo.fr"]
  volumes {
    container_path    = "/var/www/wiki"
    volume_name       = "${var.volume_path}wiki-data"
    read_only         = true
  }
  volumes {
    container_path    = "/etc/nginx/conf.d"
    volume_name       = "${var.config_path}wiki"
    read_only         = true
  }
}
