resource "docker_container" "rss" {
  image               = docker_image.rss.image_id
  name                = "rss"
  hostname            = "rss"
  restart             = "on-failure"
  env                 = [
                          "VIRTUAL_HOST=rss.adminrezo.fr",
                          "CRON_MIN=*/15"
                        ]
  volumes {
    container_path    = "/var/www/FreshRSS/data"
    volume_name       = "${var.volume_path}rss-data"
  }
}
