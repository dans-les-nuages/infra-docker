resource "docker_container" "rss-bridge" {
  image               = docker_image.rssb.image_id
  name                = "rss-bridge"
  hostname            = "rss-bridge"
  restart             = "on-failure"
  env                 = ["VIRTUAL_HOST=rss-bridge.adminrezo.fr"]
  volumes {
    container_path    = "/app/bridges"
    volume_name       = "${var.volume_path}rss-bridge-bridges"
    read_only         = true
  }
  volumes {
    container_path    = "/config"
    volume_name       = "${var.volume_path}rss-bridge-config"
    read_only         = true
  }
}
