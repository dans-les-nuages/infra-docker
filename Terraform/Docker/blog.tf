resource "docker_container" "blog" {
  image               = docker_image.nginx.image_id
  name                = "blog"
  hostname            = "blog"
  restart             = "on-failure"
  env                 = [
                          "VIRTUAL_HOST=blog.adminrezo.fr",
                          "VIRTUAL_HOST=adminrezo.fr"
                        ]
  volumes {
    container_path    = "/var/www/blog"
    volume_name       = "${var.volume_path}blog-data"
    read_only         = true
  }
  volumes {
    container_path    = "/etc/nginx/conf.d"
    volume_name       = "${var.config_path}blog"
    read_only         = true
  }
}
