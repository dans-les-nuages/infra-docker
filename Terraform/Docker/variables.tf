variable "user" {
  type    = string
  default = "pi"
}
variable "host" {
  type    = string
  default = "192.168.0.5"
}
variable "port" {
  type    = string
  default = "52222"
}
variable "volume_path" {
  type    = string
  default = "/home/pi/dans-les-nuages.fr/terraform/VOLUMES/"
}
variable "config_path" {
  type    = string
  default = "/home/pi/dans-les-nuages.fr/terraform/config/"
}
