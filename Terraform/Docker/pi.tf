resource "docker_container" "pi" {
  image               = docker_image.pi.image_id
  name                = "pi"
  hostname            = "pi"
  restart             = "on-failure"
  env                 = [ 
                          "VIRTUAL_HOST=pi.adminrezo.fr",
                          "TZ=Europe/Paris"
                        ]
  capabilities {
    add               = ["NET_ADMIN"]
  }
  volumes {
    container_path    = "/etc/pihole"
    volume_name       = "${var.volume_path}pi-data1"
    read_only         = false
  }
  volumes {
    container_path    = "/etc/dnsmasq.d"
    volume_name       = "${var.volume_path}pi-data2"
    read_only         = false
  }
  ports {
    internal          = 53
    external          = 53
  }
  ports {
    internal          = 53
    external          = 53
    protocol          = "udp"
  }
}
