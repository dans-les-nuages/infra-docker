resource "docker_image" "nginx" {
  name         = "nginx:alpine"
  keep_locally = false
}

resource "docker_image" "shaarli" {
  name         = "ghcr.io/shaarli/shaarli"
  keep_locally = false
}

resource "docker_image" "php" {
  name         = "php:8.1-apache"
  keep_locally = false
}

resource "docker_image" "pi" {
  name         = "pihole/pihole:latest"
  keep_locally = false
}

resource "docker_image" "rssb" {
  name         = "rssbridge/rss-bridge:latest"
  keep_locally = false
}

resource "docker_image" "rss" {
  name         = "freshrss/freshrss:edge"
  keep_locally = false
}

resource "docker_image" "proxy" {
  name         = "jwilder/nginx-proxy:latest"
  keep_locally = false
}
