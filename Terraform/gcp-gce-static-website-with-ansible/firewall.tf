resource "google_compute_firewall" "sws" {
  name    = "static-website-firewall"
  network = google_compute_network.sws-gce-vpc.id

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }
  source_ranges = ["0.0.0.0/0"]
}
