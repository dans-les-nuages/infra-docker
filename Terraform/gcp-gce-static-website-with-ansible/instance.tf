# Create a single Compute Engine instance
resource "google_compute_instance" "default" {
  name         = "sws-vm"
  machine_type = "e2-micro"
  zone         = var.zone
  tags         = ["web"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-12"
    }
  }

  # Install Nginx
  metadata = {
    startup-script = <<-EOF
sudo apt update
sudo apt install -yq nginx
sudo echo 'Hello world' > /usr/share/nginx/html/index.html"
EOF
  }

  network_interface {
    subnetwork = google_compute_subnetwork.sws-gce-subnet.id
    access_config { }
  }
}
