terraform {
  required_providers {
    ansible = {
      version = "1.2.0"
      source  = "ansible/ansible"
    }
  }
}

provider google {
  project     = var.project_name
  region      = var.region
  zone        = var.zone
}
