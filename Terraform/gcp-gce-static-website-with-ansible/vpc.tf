# vpc.tf

resource "google_compute_network" "sws-gce-vpc" {
  name                    = "gce-vpc"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "sws-gce-subnet" {
  name          = "gce-subnet"
  ip_cidr_range = "10.5.0.0/16"
  region        = var.region
  network       = google_compute_network.sws-gce-vpc.id
}
