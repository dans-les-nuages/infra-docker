resource "google_compute_network" "interne_nd" {
  name = "vpc-network-nd"
}

resource "google_compute_subnetwork" "interne_subnet_nd" {
  name          = "interne-subnet-nd"
  ip_cidr_range = "10.10.10.0/24"
  region        = "us-west1"
  network       = google_compute_network.interne_nd.id
}

#resource "google_service_account" "default" {
#  account_id   = "service_account_id"
#  display_name = "Service Account"
#}

resource "google_compute_instance" "instance01nd" {
  name         = "instance01nd"
  machine_type = "n1-standard-1"
  zone         = "us-west1-a"

  boot_disk {
    initialize_params {
      #image = "debian-cloud/debian-11"
      image = "ubuntu-1804-bionic-v20190429"
    }
  }

  network_interface {
    network = "vpc-network-nd"
    subnetwork = "interne-subnet-nd"
    access_config { nat_ip = google_compute_address.ip-address.address }
  }
  metadata = { sshKeys = "" }
}

resource "google_compute_address" "ip-address" {
  name = "floating-ip"
}
