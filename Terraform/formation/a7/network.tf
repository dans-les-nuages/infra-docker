# Create a VPC
resource "aws_vpc" "main" {
  count = 2
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "terra_ilki_ND_${count.index}"
    Formation = "terraform"
  }
}

# Subnet
resource "aws_subnet" "main_subnet" {
  for_each = var.deploy_vpc ? toset(var.subnets) : []
  vpc_id = aws_vpc.main[0].id
  cidr_block = each.value
}
