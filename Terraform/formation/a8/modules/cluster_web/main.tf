# Instance
resource "aws_launch_template" "template_web" {
  image_id      = "ami-06358f49b5839867c"
  instance_type = "t3.micro"
  lifecycle { create_before_destroy = true }
  tags = {
    name      = "instance-de-${local.suffix}"
    formation = "Terraform"
  }
  vpc_security_group_ids = [aws_security_group.allow_http.id]
  depends_on = [
    aws_security_group.allow_http,
  ]
  key_name  = aws_key_pair.key_pair.key_name
  user_data = base64encode(data.template_file.init.rendered)
}

resource "aws_autoscaling_group" "cluster_web_servers" {
  desired_capacity   = 2
  min_size           = 2
  max_size           = 5
  availability_zones = data.aws_availability_zones.available.names
  load_balancers     = [aws_elb.lb_web_servers.name]
  health_check_type  = "ELB"
  launch_template {
    id      = aws_launch_template.template_web.id
    version = "$Latest"
  }
  tag {
    key                 = "Name"
    value               = "cluster_web_servers_${local.suffix}"
    propagate_at_launch = true
  }
}

data "aws_availability_zones" "available" {
  state = "available"
}

data "template_file" "init" {
  template = file("./bin/user-data.sh")
  vars     = { web_content = "${var.web_content}" }
}

data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-20230424"]
  }
}
