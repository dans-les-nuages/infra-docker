locals {
  suffix = "nd"
}

variable "web_content" {
  type = string
  #default     = "Hello world"
  description = "Contenu du site Web"
}

variable "pk" {
  type        = string
  description = "Clé publique"
}

variable "public_key" {
  type        = string
  default     = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIP5kcQ2OsD4qLoW1tSCOl96ZmZ/tF0WIOxzzA/ytwBsI nico@maison"
  description = "Clé publique"
}

#variable "PUBKEY" { type = string }

#output "instance_ip" { value = aws_instance.instance01.public_ip }
#output "content" { value = var.web_content }
#output "public_key" { value = var.PUBKEY }
output "cluster_dns_access" {
  value = aws_elb.lb_web_servers.dns_name
}
