resource "aws_dynamodb_table" "state_locking_table" {
  name = "state-locking-tableND"
  billing_mode = "PROVISIONED"
  read_capacity = 20
  write_capacity = 20
  hash_key = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
  tags = {
    Name = "dynamodb-table-state-locking-ND"
    Environment = "production"
    Formation = "Terraform"
  }
}
