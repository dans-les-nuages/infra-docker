resource "aws_s3_bucket" "terraform" {
  bucket = "terraform-ilkilab-formation-nd"
  tags = {
    Name        = "terraform State File"
    Environment = "Production"
    Formation   = "Terraform"
  }
  lifecycle {
    prevent_destroy = false
  }
}

resource "aws_s3_bucket_ownership_controls" "s3_bucket_acl_ownership" {
  bucket = aws_s3_bucket.terraform.id
  rule { object_ownership = "ObjectWriter" }
}

resource "aws_s3_bucket_acl" "terraform_bucket_acl" {
  bucket     = aws_s3_bucket.terraform.id
  acl        = "private"
  depends_on = [aws_s3_bucket_ownership_controls.s3_bucket_acl_ownership]
}

resource "aws_kms_key" "key" {
  description             = "Clé pour le chiffrement des buckets Terraform"
  deletion_window_in_days = 10
  tags = {
    Formation = "Terraform"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "bucket_configuration" {
  bucket = aws_s3_bucket.terraform.bucket
  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = aws_kms_key.key.arn
      sse_algorithm     = "aws:kms"
    }
  }
}

resource "aws_s3_bucket_public_access_block" "access_block" {
  bucket                  = aws_s3_bucket.terraform.id
  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

resource "aws_s3_bucket_policy" "terraform_policy" {
  bucket = "${aws_s3_bucket.terraform.id}"
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect    = "Allow"
        Principal = "*"
        Action = [
          "s3:ListBucket",
        ]
        Resource = [
          "${aws_s3_bucket.terraform.arn}"
        ]
      },
      {
        Effect    = "Allow"
        Principal = "*"
        Action = [
          "s3:GetObject",
          "s3:PutObject",
        ]
        Resource = [
          "${aws_s3_bucket.terraform.arn}/*"
        ]
      },
    ]
    }
  )
  depends_on = [aws_s3_bucket_public_access_block.access_block]
}

resource "aws_s3_bucket_versioning" "versioning" {
  bucket = aws_s3_bucket.terraform.id
  versioning_configuration { status = "Enabled" }
}
