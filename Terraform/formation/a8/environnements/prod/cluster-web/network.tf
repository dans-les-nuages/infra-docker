# Create a VPC
resource "aws_vpc" "vpc_nd" {
  cidr_block = "10.1.0.0/16"
  lifecycle { create_before_destroy = true }
}

resource "aws_security_group" "allow_http" {
  name        = "allow-http-${local.suffix}"
  description = "Allow HTTP inbound"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    name      = "allow_http${local.suffix}"
    formation = "Terraform"
  }
}

resource "aws_key_pair" "key_pair" {
#lifecycle { create_before_destroy = true }
#key_name   = "key_pair_${local.suffix}"
  public_key = var.pk
  #public_key = file("id_ed25519.pub")
#tags = {
#    Formation = "Terraform"
#}
}

resource "aws_elb" "lb_web_servers" {
  name = "lbwebservers-${local.suffix}"
  availability_zones = data.aws_availability_zones.available.names
  security_groups = [aws_security_group.allow_http.id]
  listener {
    instance_port = 80
    instance_protocol = "http"
    lb_port = 80
    lb_protocol = "http"
  }
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    target = "HTTP:80/"
    interval = 30
  }
}
